/**
 * @file pitTheKiller.hpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-09-02
 * 
 * @copyright Copyright Spartaco Amadei (c) 2019
 * 
 */
#ifndef __PIT_THEKILLER_HPP
#define __PIT_THEKILLER_HPP

#include <sys/wait.h>
#include <unistd.h>

#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cstdio>



/**
 * @brief Handle a process and permit to launch 
 * and kill it, any object of this class can rapresent 
 * a different process and, so, permits to manage it 
 * safe respect the other process
 */
class PitTheKiller {
public:
	/**
	 * @brief Construct a new Pit The Killer object
	 * 
	 * @param exec The path of the programm to launch 
	 */
	PitTheKiller(const std::string& exec);
	~PitTheKiller() = default;

	/**
	 * @brief Create a Process Instance in a separate
	 * handlable thread and get the PID.
	 * 
	 */
	void launchThisShit();
	/**
	 * @brief Kill the process launched
	 * with createProcessInstance()
	 * 
	 */
	void killThisBastard();

	/**
	 * @brief Get the App Name
	 * 
	 * @return std::string  the name of app to launch 
	 */
    std::string getAppName() { return appName; }
	/**
	 * @brief Get PID of child proces
	 * 
	 * @return long The PID of the child process 
	 */
	long getPID() { return (long)PID; }

private:
	std::string setAppName();

    pid_t PID;
    std::string appName;
    std::string exec;
};
#endif //__PIT_THEKILLER_HPP