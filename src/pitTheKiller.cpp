/**
 * @file pitTheKiller.cpp
 * @author Spartaco Amadei (spamadei@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2019-09-02
 * 
 * @copyright Copyright Spartaco Amadei (c) 2019
 * 
 */
#include "pitTheKiller.hpp"

#include <regex>
#include <cstdlib>

//public

PitTheKiller::PitTheKiller(const std::string& exec):
    exec{exec}
{
    appName = setAppName();
}

void PitTheKiller::launchThisShit()
{
    PID = fork();
    int status;

    switch(PID) {
        case -1:     // error
            perror("fork()");
            exit(1);
        case 0:     //child process
            execl(exec.c_str(), exec.c_str(), NULL);
            perror("execl()");  //execl doesn't return unless 
                                //there is a problem
            exit(1);
        }
}

void PitTheKiller::killThisBastard()
{
    char buff[50];
    sprintf(buff, "kill -9 %d", PID);
    std::cout << "The command is:" << buff << std::endl;
    if (std::system(buff) != 0) {
        std::cerr << "Cant kill the process!\n";
    }
}

//private

std::string PitTheKiller::setAppName()
{
    std::string appName;

    std::regex r("([/])+(([a-z A-Z]+)+([^/]))");
    std::smatch match;

    if(std::regex_search(exec, match, r)) {
        appName = match[3];
    }

    return appName;
}

